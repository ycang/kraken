<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include the facebook.php from libraries directory 

class Employee extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */ 

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('employee_model');  

		if(!$this->fb_login() && $this->router->fetch_method() != 'login'){
			redirect('employee/login'); 
		}

      	$data['user_profile'] = $this->fb_login();
		$this->load->view('templates/header',$data);
	}

	public function index()
	{
		$data['employee'] = $this->employee_model->get_employee(); 
 
		$this->load->view('employee/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($id = null)
	{
		$data['employee_item'] = $this->employee_model->get_employee($id);

		if (empty($data['employee_item']))
		{
			show_404();
		} 
 
		$this->load->view('employee/view', $data);
		$this->load->view('templates/footer');
	} 

	public function edit($id = null)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');  

		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[employee.name]');
		$this->form_validation->set_rules('employment_history', 'History of Employment', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required|valid_date[y-m-d,-]');
		$this->form_validation->set_rules('joined_date', 'Joined Date', 'required|valid_date[y-m-d,-]'); 
    	$this->form_validation->set_message('is_unique', 'This %s is already exist.'); 

		if (isset($_POST['save']) && $_POST['save'] == 'edit') {

			$data = array(
				'id' => $this->input->post('id'), 
				'name' => $this->input->post('name'), 
				'dob' => $this->input->post('dob'), 
				'employment_history' => $this->input->post('employment_history'),
				'created_date' => date('Y-m-d H:i:s'),
				'joined_date' => $this->input->post('joined_date'),
			);

			if ($this->form_validation->run() === FALSE)
			{
    			$this->session->set_flashdata('err_message', validation_errors());
				redirect('employee/edit/'.$this->input->post('id'));
			}
			else
			{  
				$this->employee_model->add($data);
    			$this->session->set_flashdata('suc_message', 'Details has been updated.');
				redirect('employee/view/'.$this->input->post('id'));
			}
		}else{

			$data['employee_item'] = $this->employee_model->get_employee($id);

			if (empty($data['employee_item']))
			{
				show_404();
			} 
 
			$this->load->view('employee/edit', $data);
			$this->load->view('templates/footer'); 

		}
	} 

	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation'); 
		$this->form_validation->set_error_delimiters('<div class="error text-danger">', '</div>');

		$this->form_validation->set_rules('name', 'Name', 'required|is_unique[employee.name]');
		$this->form_validation->set_rules('employment_history', 'History of Employment', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required|valid_date[y-m-d,-]');
		$this->form_validation->set_rules('joined_date', 'Joined Date', 'required|valid_date[y-m-d,-]'); 
    	$this->form_validation->set_message('is_unique', 'This %s is already exist.'); 

		if (isset($_POST['save']) && $_POST['save'] == 'create') {
			$data = array(
				'name' => $this->input->post('name'), 
				'dob' => $this->input->post('dob'), 
				'employment_history' => $this->input->post('employment_history'),
				'created_date' => date('Y-m-d H:i:s'),
				'joined_date' => $this->input->post('joined_date'),
			);

			if ($this->form_validation->run() === FALSE)
			{
    			$this->session->set_flashdata('err_message', validation_errors());
				redirect('employee/create/',$data);  
			}
			else
			{
				$this->employee_model->add($data);
				$this->session->set_flashdata('suc_message', 'Details has been created.');
				redirect('employee/view/'.$this->db->insert_id());
			}
		}else{  
				$this->load->view('employee/create');
				$this->load->view('templates/footer');
		}
	}


	  /**
	   * This function deletes a book from the database
	   * and redirects back to the listing
	   * @param int $id
	   */
	  public function delete($id = null) {
	    if ($id == null) {
	      show_error('No identifier provided', 500);
	    }
	    else {
	      	$this->employee_model->remove($id);
    		$this->session->set_flashdata('suc_message', 'Employee has been deleted.');
	      	redirect('employee/index'); // back to the listing
	    }
	  }
	 
	 function fb_login(){
		$data['user_profile'] = '';
		$this->load->library('facebook'); 

		$user = $this->facebook->getUser(); 
        if ($user) { 
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }else { 
            $this->facebook->destroySession();
        }
        return $data['user_profile'];
	 }

	public function login(){ 
		
      	$data['user_profile'] = $this->fb_login();
        if ($data['user_profile']) { 
            $data['logout_url'] = site_url('employee/logout'); // Logs off application
        	redirect('employee/index');
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();

        } else { 
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('employee/login'), 
                'scope' => array("email") // permissions here
            ));
        } 
        	$this->load->view('employee/login',$data); 
			$this->load->view('templates/footer');
	}

    public function logout(){

        $this->load->library('facebook');

        // Logs off session from website
        $this->facebook->destroySession();
        // Make sure you destory website session as well.

        redirect('employee/login');
    }


	// My callback function
	public function check_duplicate_name($name=null, $id=null) {
		echo $id;die();
	    return $this->employee_model->checkDuplicateName($name,$id);

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */