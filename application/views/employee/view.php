<div id="infoMessage"><?php echo $this->session->flashdata('err_message');?></div>
<h2>Employee Details</h2>
<table class="table">
	<tr>
		<td>Name</td>
		<td><?php echo $employee_item['name'];?></td>
	</tr>
	<tr>
		<td>History of Employment</td>
		<td><?php echo $employee_item['employment_history'];?></td>
	</tr>
	<tr>
		<td>Date of Birth</td>
		<td><?php echo date('Y-m-d',strtotime($employee_item['dob']));?></td>
	</tr>
	<tr>
		<td>Joined Date</td>
		<td><?php echo date('Y-m-d',strtotime($employee_item['joined_date']));?></td>
	</tr>
	<tr>
		<td>Created Date</td>
		<td><?php echo date('Y-m-d',strtotime($employee_item['created_date']));?></td>
	</tr>
</table>

<a href="<?php echo site_url("employee/index");?>" class="btn btn-primary" role="button">Back</a>
<a href="<?php echo site_url("employee/delete/".$employee_item['id']);?>" class="btn btn-danger" role="button">Delete</a>
<a href="<?php echo site_url("employee/edit/".$employee_item['id']);?>" class="btn btn-warning" role="button">Edit</a>