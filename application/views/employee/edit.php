<h2>Edit Employee Details</h2> 

<?php echo form_open('employee/edit') ?>
	<input type="hidden" name="id" value="<?php echo $employee_item['id'];?>"/> 
	<input type="hidden" name="save" value="edit"/> 
	<div class="form-group">
		<label for="name">Name</label>
		<input type="input" class="form-control" name="name" value="<?php echo $employee_item['name'];?>"/><br />
	</div>

	<div class="form-group">
		<label for="employment_history">History of Employment</label>
		<input type="input" class="form-control" name="employment_history" value="<?php echo $employee_item['employment_history'];?>"/><br />
	</div>

	<div class="form-group">
		<label for="dob">Date of Birth</label>
		<input type="input" class="datepicker form-control"  data-date-format="yyyy-mm-dd"  name="dob" value="<?php echo date('Y-m-d',strtotime($employee_item['dob']));?>"/><br />
	</div>

	<div class="form-group">
		<label for="joined_date">Joined Date</label>
		<input type="input" class="datepicker form-control"  data-date-format="yyyy-mm-dd" name="joined_date" value="<?php echo date('Y-m-d',strtotime($employee_item['joined_date']));?>"/><br />
	</div>

<a href="<?php echo site_url("employee/index");?>" class="btn btn-primary" role="button">Back</a>
	<button type="submit" class="btn btn-primary">Update</button>
</form>  

