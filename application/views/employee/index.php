
<h2>Employees</h2>
<a href="<?php echo site_url("employee/create");?>" class="btn btn-success pull-right" role="button">Add New Employee</a>
<table class="table">
	<thead>
		<tr>
			<td>Name</td>
			<td>History of Employment</td>
			<td>D.O.B.</td>
			<td>Joined Date</td>
			<td>Created Date</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($employee as $employee_item): ?>
		<tr>
			<td><?php echo $employee_item['name'] ?></td>
			<td><?php echo $employee_item['employment_history'] ?></td>
			<td><?php echo date('Y-m-d',strtotime($employee_item['dob'])); ?></td>
			<td><?php echo date('Y-m-d',strtotime($employee_item['joined_date'])); ?></td>
			<td><?php echo date('Y-m-d',strtotime($employee_item['created_date'])); ?></td>
			<td><p><a href="<?php echo site_url("employee/view/".$employee_item['id'])?>">View Employee</a></p></td>
		</tr> 
	<?php endforeach ?>
	</tbody>
</table>   