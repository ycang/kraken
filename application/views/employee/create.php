
<h2>Create a New Employee</h2> 

<?php echo form_open('employee/create') ?>
	<input type="hidden" name="save" value="create"/> 
	<div class="form-group">
		<label for="name">Name</label>
		<input type="input" class="form-control" name="name" /><br />
	</div>

	<div class="form-group">
		<label for="employment_history">History of Employment</label>
		<input type="input" class="form-control" name="employment_history" value="<?php echo (isset($name))?$name:'';?>" /><br />
	</div>

	<div class="form-group">
		<label for="dob">Date of Birth</label> 
	  	<input class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" name="dob" value="">
	 </div>

	<div class="form-group">
		<label for="joined_date">Joined Date</label>
		<input class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" name="joined_date" value="">
	</div>

	<a href="<?php echo site_url("employee/index");?>" class="btn btn-primary" role="button">Back</a>
	<button type="submit" class="btn btn-primary">Create</button>
</form>  

