<?php
class Employee_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_employee($id = FALSE)
	{
		if ($id === FALSE)
		{
			$query = $this->db->get('employee');
			return $query->result_array();
		}

		$query = $this->db->get_where('employee', array('id' => $id));
		return $query->row_array();
	}

	public function add($data) {
		if (isset($data['id'])) {
		  $this->db->where('id', $data['id']);
		  $this->db->update('employee',$data); // update the record
		}
		else {
		  $this->db->insert('employee', $data); // insert new record
		}
	} 
 
	/**
	* This function will delete the record based on the id
	* @param $id
	*/
	public function remove($id) {
		$this->db->where('id', $id);
		$this->db->delete('employee');
	}

	public function checkDuplicateName($name,$id) {
		
	    $this->db->where('name', $name);
	    if($id){ 
	   		$this->db->where('id !=', $id); 
	    }
	    $query = $this->db->get('employee');

	    $count_row = $query->num_rows();

	    if ($count_row > 0) {
	      //if count row return any row; that means you have already this email address in the database. so you must set false in this sense.
	        return FALSE; // here I change TRUE to false.
	     } else {
	      // doesn't return any row means database doesn't have this email
	        return TRUE; // And here false to TRUE
	     }
	}
	 
}