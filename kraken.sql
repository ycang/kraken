-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2014 at 08:59 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kraken`
--
CREATE DATABASE IF NOT EXISTS `kraken` DEFAULT CHARACTER SET utf32 COLLATE utf32_unicode_ci;
USE `kraken`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf32_unicode_ci NOT NULL,
  `dob` datetime NOT NULL,
  `employment_history` text COLLATE utf32_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `joined_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `dob`, `employment_history`, `created_date`, `joined_date`) VALUES
(1, 'Ang Yeow Chuans', '1970-01-01 00:00:00', 'Kraken', '2014-07-09 08:55:14', '2014-07-10 00:00:00'),
(3, 'test', '0000-00-00 00:00:00', 'test', '2014-07-08 09:06:08', '0000-00-00 00:00:00'),
(8, 'Ang Yeow Chuan', '1970-01-01 00:00:00', 'd', '2014-07-08 10:35:26', '1970-01-01 00:00:00'),
(10, 'young_89', '2014-07-08 00:00:00', 'abc', '2014-07-09 08:48:27', '2014-07-11 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
